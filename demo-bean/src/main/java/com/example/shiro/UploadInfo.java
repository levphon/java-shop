package com.example.shiro;
import java.io.Serializable;

/**
 * @author
 */
public class UploadInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    //上传结果
    private String result;
    //初始文件名
    private String beginFileName;
    //最终上传文件名
    private String lastFileName;
    //文件类型
    private String fileType;
    //文件大小
    private String fileSize;
    //文件上传的地址
    private String uploadUrl;
    public String getResult() {
        return result;
    }

    public String getBeginFileName() {
        return beginFileName;
    }

    public String getLastFileName() {
        return lastFileName;
    }

    public String getFileType() {
        return fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }
    public void setResult(String result) {
        this.result = result;
    }

    public void setBeginFileName(String beginFileName) {
        this.beginFileName = beginFileName;
    }

    public void setLastFileName(String lastFileName) {
        this.lastFileName = lastFileName;
    }

    public void setFileType(String fileType) {
        fileType = fileType;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }
    @Override
    public String toString() {
        return "UploadInfo{" +
                "result='" + result + '\'' +
                ", beginFileName='" + beginFileName + '\'' +
                ", lastFileName='" + lastFileName + '\'' +
                ", FileType='" + fileType + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", uploadUrl='" + uploadUrl + '\'' +
                '}';
    }
}