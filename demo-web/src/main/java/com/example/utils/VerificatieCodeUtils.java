package com.example.utils;

import java.util.Random;
/**
 * @description:
 * @author: xxx
 * @create: 2022/11/2 11:44
 */

public class VerificatieCodeUtils {
    public static String getRandNum(int charCount) {
        String charValue = "";
        for (int i = 0; i < charCount; i++) {
            char c = (char) (randomInt(0, 10) + '0');
            charValue += String.valueOf(c);
        }
        return charValue;
    }

    public static int randomInt(int from, int to) {
        Random r = new Random();
        return from + r.nextInt(to - from);
    }
}
