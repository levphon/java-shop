package com.example.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: xxx
 * @create: 2022/4/19 10:46
 */
@Component
public class RedisUtil {
    @Autowired
    private RedisTemplate redisTemplate;
//    @Bean
//    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
//        RedisTemplate<Object, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(connectionFactory);
//
//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//
//        //指定键的序列化器
//        template.setKeySerializer(new StringRedisSerializer());
//        //指定值的序列化器
//        template.setValueSerializer(jackson2JsonRedisSerializer);
//        template.afterPropertiesSet();
//
//        return template;
//
//    }
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void set(String key, String value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    public void setMap(String key, Map<String, String> value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }
    public void setMap(String key, Map<String, String> value) {
        redisTemplate.opsForValue().set(key, value);
    }
    public void setObjMap(String key, Map<String, Object> value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value);
    }
    public Map<String, Object> getObjMap(String key) {
        return (Map<String, Object>) redisTemplate.opsForValue().get(key);
    }
    public Object get(String key) {
        Object value = redisTemplate.opsForValue().get(key);
        return value;
    }

    public Map<String, String> getMap(String key) {
        Map<String, String> value = (Map<String, String>) redisTemplate.opsForValue().get(key);
        return value;
    }

    public void addSet(String key, Object value) {
        redisTemplate.opsForSet().add(key, value);
    }

    public Set getSet(String key) {
        Set set = redisTemplate.opsForSet().members(key);
        return set;
    }

    public Long getSetLen(String key) {
        long setLength = redisTemplate.opsForSet().size(key);
        return setLength;
    }

    public void delete(String key){
        redisTemplate.delete(key);
    }

}
