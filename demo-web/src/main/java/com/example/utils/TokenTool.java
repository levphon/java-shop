package com.example.utils;

import com.example.shiro.util.ShiroUtils;
import org.apache.shiro.session.Session;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public class TokenTool {

	/**
	 * 获取用户ID
	 * @return
	 */
    public static String getUserId() {

        Session session = ShiroUtils.getSession();
        System.out.println("id"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String userId = null;
        if(userInfo!=null){
            userId = userInfo.get("id").toString();
        }
        return userId;
    }

     /**
  	 * 获取用户角色所属
  	 * @return
  	 */
      public static List<Map<String, Object>> getRole() {

          Session session = ShiroUtils.getSession();
          System.out.println("sessionId"+session.getId());
          List<Map<String, Object>> roleList = (List<Map<String, Object>>) session.getAttribute("roleList");

          return roleList;
      }

    /**
     * getUrlById
     * 获取用户角色所属:m：超级管理员，h:画室管理员，p普通用户
     * @return
     */
    public static String getPermissionType() {

        Session session = ShiroUtils.getSession();
        System.out.println("getUserId"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String type = null;
        if(userInfo!=null){
            type = userInfo.get("type").toString();
        }
        return type;
    }
    public static String getVip() {

        Session session = ShiroUtils.getSession();
        System.out.println("getUserId"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String vip = null;
        if(userInfo!=null){
            vip = userInfo.get("vip").toString();
        }
        return vip;
    }
    /**
     *
     * 获取当前选择的画室Id
     * @return
     */
    public static String getpId() {

        Session session = ShiroUtils.getSession();
        System.out.println("getUserId"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String pid = null;
        if(userInfo!=null){
            pid = userInfo.get("pid").toString();
        }
        return pid;
    }
    /**
     * 获取request
     *
     * @return
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        return requestAttributes == null ? null : requestAttributes.getRequest();
    }


}
