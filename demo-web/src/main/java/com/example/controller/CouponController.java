package com.example.controller;

import com.example.service.CouponService;
import com.example.service.OrderService;
import com.example.service.UserService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @description: 电子券
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("coupon")
public class CouponController {

    @Autowired
    private CouponService service;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * @description: 我的电子券列表
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getMyList")
    public Result getList(@RequestBody Map<String,Object> map) {
        map.put("userId", TokenTool.getUserId());
        return ResultUtil.success(service.getMyList(map));
    }
    /**
     * @description:电子券数量；今日购买数量；获取注册天数
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/11/4 16:08
     */
    @RequestMapping("getMyInfo")
    public Result getMyInfo(@RequestBody Map<String,Object> map) {

        Map<String,Object> result = new HashMap<>();
        map.put("userId", TokenTool.getUserId());
        result.put("couponNum",service.getMyCount(map));
        result.put("todayCount",orderService.getTodayCount(map));
        result.put("days",userService.getDays(map));
        return ResultUtil.success(result);
    }
    /**
     * @description: 获取vip信息
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/11/4 16:09
     */
    @RequestMapping("selectVipByUserId")
    public Result selectVipByUserId(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(userService.selectVipByUserId(TokenTool.getUserId()));
    }

    /**
     * @description: 获取vip信息
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/11/4 16:09
     */
    @RequestMapping("setVip")
    public Result setVip(@RequestBody Map<String,Object> map) {
        if (TokenTool.getPermissionType().equals("t")){
            List<String> userIds = Arrays.asList(map.get("ids").toString().split(","));
            userService.updateVip(map.get("endTime").toString(),userIds);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }

}
