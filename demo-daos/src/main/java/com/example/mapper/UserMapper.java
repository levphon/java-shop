package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import com.example.shiro.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: xxx
 * @create: 2022/4/14 14:05
 */
@Mapper
public interface UserMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);
    Integer getCount(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getListNew(@Param("jsonObject") JSONObject jsonObject);
    Integer getCountNew(@Param("jsonObject") JSONObject jsonObject);
    Integer getDays(Map<String, Object> map);
    Map<String, Object> getById(Map<String, Object> map);
    Map<String, Object> getByPid(Map<String, Object> map);
    Map<String, Object> getByStudioId(Map<String, Object> map);
    List<Map<String, Object>> getByCode(Map<String, Object> map);
    SysUserEntity getUserByNameNew(Map<String, Object> map);
    SysUserEntity getUserByName(String name);
    Map<String, Object> selectUserByUserName(String name);
    Map<String, Object> selectVipByUserId(String id);
    SysUserEntity getUserByNameGld(String name);
    void saveRegister(Map<String,Object> map);
    void deleteSignature(Map<String,Object> map);
    void update(Map<String,Object> map);
    void updateByName(Map<String,Object> map);
    void updateVip(@Param("endTime")String endTime,@Param("list")List<String> ids);
}
