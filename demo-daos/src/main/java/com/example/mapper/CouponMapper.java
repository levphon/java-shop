package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: xxx
 * @create: 2021/7/19 17:24
 */
@Mapper
public interface CouponMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);
    Integer getCount(@Param("jsonObject") JSONObject jsonObject);
    Integer getMyCount(Map<String, Object> map);
    List<Map<String, Object>> getMyList(Map<String, Object> map);
    Map<String, Object> getById(Map<String, Object> map);
    Map<String, Object> getMyById(Map<String, Object> map);
    void save(Map<String, Object> map);
    void saveCouponDetail(List<Map<String,Object>> mapList);
    void update(Map<String, Object> map);
    void updateCouponDetail(Map<String, Object> map);
    void delete(Map<String, Object> map);
}
