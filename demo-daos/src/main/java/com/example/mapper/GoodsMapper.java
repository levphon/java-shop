package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: xxx
 * @create: 2021/7/19 17:24
 */
@Mapper
public interface GoodsMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getTimeList(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getPreSale(Map<String, Object> map);
    List<Map<String, Object>> getListSale(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getListGoodsAdded(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getListGoodsAddedEarnings(@Param("jsonObject") JSONObject jsonObject);
    Integer getCount(@Param("jsonObject") JSONObject jsonObject);
    Integer getCountSale(@Param("jsonObject") JSONObject jsonObject);
    Integer getCountGoodsAdded(@Param("jsonObject") JSONObject jsonObject);
    Integer getCountGoodsAddedEarnings(@Param("jsonObject") JSONObject jsonObject);
    Map<String, Object> getById(Map<String, Object> map);
    Map<String, Object> getTimeById(Map<String, Object> map);
    Integer getTimeByNow();
    void save(Map<String, Object> map);
    void update(Map<String, Object> map);
    void updateTime(Map<String, Object> map);
    void delete(Map<String, Object> map);
    void saveGoodsAdded(Map<String, Object> map);
    Map<String, Object> getGoodsAddedById(Map<String, Object> map);
    Map<String, Object> getEarningsToday(Map<String, Object> map);
    Map<String, Object> getEarningsAll(Map<String, Object> map);
    void updateGoodsAdded(Map<String, Object> map);
    Integer updateGoodsAddedEarnings(Map<String, Object> map);
}
