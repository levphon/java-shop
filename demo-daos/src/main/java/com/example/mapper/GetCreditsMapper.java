package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description: 管理积分获取的记录
 * @author: xxx
 * @create: 2021/7/19 17:24
 */
@Mapper
public interface GetCreditsMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);

    Integer getCount(@Param("jsonObject") JSONObject jsonObject);

    Map<String, Object> getById(Map<String, Object> map);

    //  积分消耗记录添加
    void saveExpend(Map<String, Object> map);

    // 上架商品成功添加积分记录
    void saveAdd(Map<String, Object> map);

    // 邀请人消费成功 添加积分记录
    void saveInviteAdd(Map<String, Object> map);

    // 撤销兑换返还
    void saveRefund(Map<String, Object> map);

    // 获取我的积分
    Map<String, Object> getMyCredits(Map<String, Object> map);

    // 获取我的数据
    Map<String, Object> getMyData(Map<String, Object> map);

    void update(Map<String, Object> map);

    void delete(Map<String, Object> map);
}
