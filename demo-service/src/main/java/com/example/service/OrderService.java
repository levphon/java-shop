package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.GoodsMapper;
import com.example.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @description: 订单
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class OrderService {
    @Autowired
    private OrderMapper mapper;
    @Autowired
    private GoodsMapper goodsMapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){
        return mapper.getList(jsonObject);
    }
    public List<Map<String, Object>> getWeekList(JSONObject jsonObject){
        return mapper.getWeekList(jsonObject);
    }
    public List<Map<String, Object>> getListSale(JSONObject jsonObject){
        return mapper.getListSale(jsonObject);
    }
    public List<Map<String, Object>> getListPrice(List<String> ids){
        return mapper.getListPrice(ids);
    }
    public Map<String, Object> getSumMoney(Map<String,Object> map){
        return mapper.getSumMoney(map);
    }
    public List<Map<String, Object>> getOrderCount(JSONObject jsonObject){
        return mapper.getOrderCount(jsonObject);
    }
    public Integer getOrderCounts(JSONObject jsonObject){
        return mapper.getOrderCounts(jsonObject);
    }
    public List<Map<String, Object>> getFansListBuy(List<String> ids){
        return mapper.getListPrice(ids);
    }
    public List<Map<String, Object>> getFansListSale(List<String> ids){
        return mapper.getListPrice(ids);
    }
    public List<Map<String, Object>> getCommissionList(JSONObject jsonObject){
        return mapper.getCommissionList(jsonObject);
    }
    public Integer getCommissionCount(JSONObject jsonObject){

        return mapper.getCommissionCount(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){

        return mapper.getCount(jsonObject);
    }
    public Integer getWeekCount(JSONObject jsonObject){

        return mapper.getWeekCount(jsonObject);
    }
    public Integer getTodayCount(Map<String,Object> map){

        return mapper.getTodayCount(map);
    }
    public void saveOrder(Map<String,Object> map){
        mapper.save(map);
        Map<String,Object> mapNew = new HashMap<>();
        mapNew.put("id",map.get("goodsId"));
        mapNew.put("added",3);
        goodsMapper.update(mapNew);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return mapper.getById(map);
    }
    public Map<String, Object> getOrderAddById(String id){
        return mapper.getOrderAddById(id);
    }

    public Map<String, Object> getOrderAddByGoodsAddId(Map<String,Object> map){
        return mapper.getOrderAddByGoodsAddId(map);
    }
    public void save(Map<String,Object> map){
        mapper.save(map);
    }
    public void saveOrderAddLog(Map<String,Object> map){
        mapper.saveOrderAddLog(map);
    }
    public void updateOrder(Map<String,Object> map){
        mapper.update(map);
        Map<String,Object> mapNew = new HashMap<>();
        mapNew.put("id",map.get("goodsId"));
        mapNew.put("added",1);
        goodsMapper.update(mapNew);
    }
    public void update(Map<String,Object> map){
        mapper.update(map);
        Map<String,Object> mapNew = new HashMap<>();
        mapNew.put("id",map.get("goodsId"));
        mapNew.put("added",1);
        goodsMapper.update(mapNew);
    }
    public void updateOrderAdd(Map<String,Object> map){
        mapper.updateOrderAdd(map);
    }
    public void updateNew(Map<String,Object> map){
        mapper.update(map);
        Map<String,Object> mapNew = new HashMap<>();
        mapNew.put("id",map.get("goodsId"));
        mapNew.put("added",1);
        goodsMapper.update(mapNew);
    }
    public void delete(Map<String,Object> map){
        mapper.delete(map);
    }


}
