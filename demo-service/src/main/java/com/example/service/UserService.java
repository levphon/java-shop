package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.UserMapper;
import com.example.shiro.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: xxx
 * @create: 2022/4/14 15:30
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){
        jsonObject.put("type","m");
        return userMapper.getList(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){
        jsonObject.put("type","m");
        return userMapper.getCount(jsonObject);
    }
    public Integer getDays(Map<String,Object> map){
        return userMapper.getDays(map);
    }
    public Integer getCountNew(JSONObject jsonObject){
        return userMapper.getCountNew(jsonObject);
    }
    public List<Map<String, Object>> getListNew(JSONObject jsonObject){
        return userMapper.getListNew(jsonObject);
    }
    public SysUserEntity getUserByNameNew(Map<String, Object> map){
        return userMapper.getUserByNameNew(map);
    }
    public SysUserEntity getUserByName(String name){
        return userMapper.getUserByName(name);
    }
    public Map<String, Object> selectUserByUserName(String name){
        return userMapper.selectUserByUserName(name);
    }
    public SysUserEntity getUserByNameGld(String name){
        return userMapper.getUserByNameGld(name);
    }
    public Map<String, Object> selectVipByUserId(String id){
        return userMapper.selectVipByUserId(id);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return userMapper.getById(map);
    }
    public Map<String, Object> getByPid(Map<String,Object> map){
        return userMapper.getByPid(map);
    }
    public List<Map<String, Object>> getByCode(Map<String,Object> map){
        return userMapper.getByCode(map);
    }
    public void saveRegister(Map<String,Object> map){
        userMapper.saveRegister(map);
    }
    public void deleteSignature(Map<String,Object> map){
        userMapper.deleteSignature(map);
    }
    public void update(Map<String,Object> map){
        userMapper.update(map);
    }
    public void updateVip(String endTime,List<String> ids){
        userMapper.updateVip(endTime,ids);
    }

    public void updateByName(Map<String,Object> map){
        userMapper.updateByName(map);
    }
    public Map<String, Object> getByStudioId(Map<String,Object> map){
        return userMapper.getByStudioId(map);
    }
}
