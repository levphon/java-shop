package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.CouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 优惠券
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class CouponService {
    @Autowired
    private CouponMapper mapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){

        return mapper.getList(jsonObject);
    }
    public List<Map<String, Object>> getMyList(Map<String,Object> map){
        return mapper.getMyList(map);
    }

    public Integer getCount(JSONObject jsonObject){

        return mapper.getCount(jsonObject);
    }
    public Integer getMyCount(Map<String,Object> map){

        return mapper.getMyCount(map);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return mapper.getById(map);
    }
    public Map<String, Object> getMyById(Map<String,Object> map){
        return mapper.getMyById(map);
    }
    public void save(Map<String,Object> map){
        mapper.save(map);
    }
    public void saveCouponDetail(List<Map<String,Object>> mapList){
        mapper.saveCouponDetail(mapList);
    }
    public void update(Map<String,Object> map){
        mapper.update(map);
    }
    public void updateCouponDetail(Map<String,Object> map){
        mapper.updateCouponDetail(map);
    }
    public void delete(Map<String,Object> map){
        mapper.delete(map);
    }

}
