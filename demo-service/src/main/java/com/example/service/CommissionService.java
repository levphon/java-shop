package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.CommissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 佣金
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class CommissionService {
    @Autowired
    private CommissionMapper mapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){

        return mapper.getList(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){

        return mapper.getCount(jsonObject);
    }
    public List<Map<String, Object>> getWithdrawList(JSONObject jsonObject){

        return mapper.getWithdrawList(jsonObject);
    }
    public Integer getWithdrawCount(JSONObject jsonObject){

        return mapper.getWithdrawCount(jsonObject);
    }
    public Map<String, Object> getWithdrawById(Map<String,Object> map){
        return mapper.getWithdrawById(map);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return mapper.getById(map);
    }
    public void saveWithdraw(Map<String,Object> map){
        mapper.saveWithdraw(map);
    }
    public void save(Map<String,Object> map){
        mapper.save(map);
    }
    public Integer update(Map<String,Object> map){
        return mapper.update(map);
    }
    public Integer updateWithdraw(Map<String,Object> map){
        return mapper.updateWithdraw(map);
    }
    public void saveCommissionLog(Map<String, Object> map){
        mapper.saveCommissionLog(map);
    }
    public void delete(Map<String,Object> map){
        mapper.delete(map);
    }

}
