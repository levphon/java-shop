package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.CreditsShopsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 积分商城商品----可兑换商品
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class CreditsShopsService {
    @Autowired
    private CreditsShopsMapper mapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject) {
        jsonObject.put("del", "1");
        return mapper.getList(jsonObject);
    }

    public Integer getCount(JSONObject jsonObject) {
        jsonObject.put("del", "1");
        return mapper.getCount(jsonObject);
    }

    public Map<String, Object> getById(Map<String, Object> map) {
        return mapper.getById(map);
    }

    public void save(Map<String, Object> map) {
        mapper.save(map);
    }

    public void update(Map<String, Object> map) {
        mapper.update(map);
    }

    public void delete(Map<String, Object> map) {
        mapper.delete(map);
    }


}
