package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.GoodsService;
import com.example.service.GoodsStatusService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 申请商品封存
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("goodsStatus")
public class GoodsStatusController {

    @Autowired
    private GoodsStatusService service;
    @Autowired
    private GoodsService goodsService;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {
        return ResultUtil.success(service.getById(map));
    }


    /**
     * @description: 同意
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("agree")
    @Transactional(rollbackFor = Exception.class)
    public Result agree(@RequestBody Map<String,Object> map) {
            Map<String,Object> mapGoods = service.getById(map);
            if (mapGoods.get("status").toString().equals("1")){
                map.put("status",2);
                service.update(map);
                return ResultUtil.success();
            }else{
                return ResultUtil.error(-500,"不允许重复审核");
            }

    }
    /**
     * @description: 驳回
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("unAgree")
    @Transactional(rollbackFor = Exception.class)
    public Result unAgree(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapGoods = service.getById(map);
        if (mapGoods.get("status").toString().equals("1")){
            map.put("status",3);
            service.update(map);
            Map<String,Object> mapNew = new HashMap<>();
            mapNew.put("id",mapGoods.get("goodsId"));
            mapNew.put("added",mapGoods.get("added"));
            goodsService.update(mapNew);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"不允许重复审核");
        }
    }
    /**
     * @description: 删除
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String,Object> map) {
            service.delete(map);
            return ResultUtil.success();
    }
}
