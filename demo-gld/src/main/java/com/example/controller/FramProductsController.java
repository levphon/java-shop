package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.FramProductsService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * @description: 助农计划-- 积分平台
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("framproducts")
public class FramProductsController {

    @Autowired
    private FramProductsService service;

    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        return ResultUtil.success(service.getList(jsonObject));
    }
    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 保存
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    public Result save(@RequestBody Map<String,Object> map) {

        map.put("id", IDTool.getUUID32());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        map.put("userId", TokenTool.getUserId());
        service.save(map);
        return ResultUtil.success();
    }
    /**
     * @description: 修改
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String,Object> map) {
        service.update(map);
        return ResultUtil.success();
    }
    /**
     * @description: 删除
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String,Object> map) {
        service.delete(map);
        return ResultUtil.success();
    }
}
