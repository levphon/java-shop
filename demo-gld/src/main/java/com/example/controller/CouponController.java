package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.CouponService;
import com.example.utils.IDTool;
import com.example.utils.Page;
import com.example.utils.Result;
import com.example.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @description: 电子券
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("coupon")
public class CouponController {

    @Autowired
    private CouponService service;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 保存
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    public Result save(@RequestBody Map<String,Object> map) {

            map.put("id", IDTool.getUUID32());
            service.save(map);
            return ResultUtil.success();
    }
    /**
     * @description: 发放电子券
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("giveCoupon")
    public Result giveCoupon(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapCoupon = new HashMap<>();
        mapCoupon.put("id",map.get("couponId"));
        Map<String,Object> coupon = service.getById(mapCoupon);
        if (coupon != null){
            List<String> userIds =Arrays.asList(map.get("ids").toString().split(","));
            List<Map<String,Object>> mapList = new ArrayList<>();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            //增加7天
            cal.add(Calendar.DAY_OF_MONTH, Integer.parseInt(coupon.get("day").toString()));
            //Calendar转为Date类型
            Date date=cal.getTime();
            for (String userId : userIds) {
                Map<String,Object> couponDetail = new HashMap<>();
                couponDetail.put("id", IDTool.getUUID32());
                couponDetail.put("userId", userId);
                couponDetail.put("couponId", map.get("couponId"));
                couponDetail.put("endTime", date);
                mapList.add(couponDetail);
            }
            service.saveCouponDetail(mapList);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"电子券不存在！");
        }



    }
    /**
     * @description: 修改
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    public Result update(@RequestBody Map<String,Object> map) {
            service.update(map);
            return ResultUtil.success();
    }

    /**
     * @description: 删除
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String,Object> map) {
            service.delete(map);
            return ResultUtil.success();
    }
}
