package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.CommissionService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @description: 提现记录
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("withdrawPage")
public class WithdrawPageController {

    @Autowired
    private CommissionService service;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")){
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        Page.getPage(jsonObject);
        jsonObject.put("status",1);
        List<Map<String, Object>> list = service.getWithdrawList(jsonObject);
        int total = service.getWithdrawCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPageNew")
    public Result getPageNew(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")){
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        Page.getPage(jsonObject);
        jsonObject.put("statusIn",1);
        List<Map<String, Object>> list = service.getWithdrawList(jsonObject);
        int total = service.getWithdrawCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")){
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        return ResultUtil.success(service.getWithdrawList(jsonObject));
    }


}
