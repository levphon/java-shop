package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.UserService;
import com.example.service.UserStatusLogService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 冻结用户
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("userStatusLog")
public class UserStatusLogController {

    @Autowired
    private UserStatusLogService service;
    @Autowired
    private UserService userService;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")){
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }

        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")){
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 保存
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    public Result save(@RequestBody Map<String,Object> map) {
        if(TokenTool.getPermissionType().equals("t")){
            map.put("id", IDTool.getUUID32());
            map.put("studioId",TokenTool.getHszStudioId());
            map.put("createUser", TokenTool.getUserId());
            service.save(map);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }

    }
    /**
     * @description: 修改
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("update")
    @Transactional(rollbackFor = Exception.class)
    public Result update(@RequestBody Map<String,Object> map) {
        if(TokenTool.getPermissionType().equals("m")){
            Map<String, Object> objectMap = service.getById(map);
            map.put("status",2);
            service.update(map);
            Map<String,Object> mapUser = new HashMap<>();
            mapUser.put("id",objectMap.get("userId").toString());
            mapUser.put("state","PROHIBIT");
            userService.update(mapUser);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }
    /**
     * @description: 解冻
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("updateState")
    @Transactional(rollbackFor = Exception.class)
    public Result updateState(@RequestBody Map<String,Object> map) {

        if(TokenTool.getPermissionType().equals("m")){
            Map<String,Object> mapUser = new HashMap<>();
            mapUser.put("id",map.get("userId").toString());
            mapUser.put("state","NORMAL");
            userService.update(mapUser);
            service.delete(map);
            return ResultUtil.success();
        }else if(TokenTool.getPermissionType().equals("t")){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId",map.get("userId").toString());
            Map<String, Object> objectMap = service.getList(jsonObject).get(0);
            if (objectMap.get("createUser").toString().equals(TokenTool.getUserId())){//谁冻结谁解冻
                Map<String,Object> mapUser = new HashMap<>();
                mapUser.put("id",objectMap.get("userId").toString());
                mapUser.put("state","NORMAL");
                userService.update(mapUser);
                service.delete(map);
            }else{
                return ResultUtil.error(-500,"无权限");
            }

            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }
    /**
     * @description: 删除
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapNew = service.getById(map);
        if (mapNew.get("status").toString().equals("1") && mapNew.get("createUser").toString().equals(TokenTool.getUserId())) {//谁冻结谁解冻
            service.delete(map);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"无权限");
        }
    }
}
