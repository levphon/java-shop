package com.example.common;

import com.example.shiro.UploadInfo;
import com.example.utils.DateUtil;
import com.example.utils.Result;
import com.example.utils.ResultUtil;
import com.example.utils.UploadFileUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j

public class FileController {
    private final static Logger logger = LoggerFactory.getLogger(FileController.class);
    @Value("${file.parentPath}")
    private String parentPath;
    @Value("${file.url}")
    private String url;

    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    @ResponseBody
    public void upload(@RequestParam("file") MultipartFile file){
        byte[] byt = null;
        try {
            byt = new byte[file.getInputStream().available()];
            file.getInputStream().read(byt);
            String res = new BASE64Encoder().encodeBuffer(byt);

            byte[] buffer = new BASE64Decoder().decodeBuffer(res);
            File packageFile = new File(parentPath);
            if (!packageFile.exists()) {
                packageFile.mkdir();
            }
            FileOutputStream out = new FileOutputStream(parentPath + "\\"+ DateUtil.getFormatData(DateUtil.DEFAULT_FORMAT_UUID) + file.getOriginalFilename());
            System.out.println(parentPath + "\\"+ DateUtil.getFormatData(DateUtil.DEFAULT_FORMAT_UUID) + file.getOriginalFilename());
            out.write(buffer);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    @ResponseBody
    public Result uploadFile(MultipartRequest request){

            List<UploadInfo> uploadInfos = new ArrayList<>();
            List<MultipartFile> multipartFiles = new ArrayList<>();


            try {

                if (request.getFile("file")!=null){
                    multipartFiles.add(request.getFile("file"));
                }
                if (multipartFiles != null && multipartFiles.size()>0){
                    uploadInfos =  UploadFileUtil.upload(multipartFiles,parentPath,url);
                }


            } catch (Exception e) {
                e.printStackTrace();
                return ResultUtil.error(-500,"上传失败");
            }

            return ResultUtil.success(uploadInfos);
    }
    @RequestMapping(value = "/uploadFiles",method = RequestMethod.POST)
    @ResponseBody
    public Result uploadFiles(@RequestParam("files") MultipartFile[] files){

        List<UploadInfo> uploadInfos = new ArrayList<>();
        List<MultipartFile> multipartFiles = new ArrayList<>();


        try {
            if(files != null && files.length > 0){
                //遍历文件
                for (MultipartFile multipartFile : files) {
                    multipartFiles.add(multipartFile);
                }
            }
            if (multipartFiles != null && multipartFiles.size()>0){
                uploadInfos =  UploadFileUtil.upload(multipartFiles,parentPath,url);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error(-500,"上传失败");
        }

        return ResultUtil.success(uploadInfos);
    }
}
